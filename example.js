import React, { Component } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { WebView } from 'react-native-webview';

export default class Sample extends Component {
  constructor(props) {
    super(props);
  }

  // sendDataFromReactNativeToWebView() {
  //   let injectedData = `document.getElementById("nameid").value = 'Nupur Sharma';
  //                       document.getElementById("emailid").value = 'nupur@gmail.com';
  //                       document.getElementById("subjectid").value = 'Test';
  //                       document.getElementById("messageid").value = 'Testing';
  //                       document.querySelector(".wpcf7-submit").click()`;
  //   this.webView.injectJavaScript(injectedData);
  // }

  sendDataFromReactNativeToWebView() {
    let injectedData = `document.querySelector('#a90000603 ul:nth-child(2) li a').click();`;
    this.webView.injectJavaScript(injectedData);
  }

  ckecked() {
    let injectJs = `document.getElementById('elogbookPrevious').checked = false;
    document.getElementById('elbScheduledNonScheduled').checked = true;`;
    this.webView.injectJavaScript(injectJs);
  }


  render() {
    const injectJs = `setTimeout(function() { document.querySelector('#a90000603 ul:nth-child(2) li a').click() },2000);
                      setTimeout(function() { document.getElementById('elbScheduledNonScheduled').click() },6000);
                     `;
    return (
      <View style={{ flex: 1, marginTop: 30 }}>

        <TouchableHighlight style={{ padding: 10, backgroundColor: 'gray', marginTop: 20 }} onPress={() => {this.sendDataFromReactNativeToWebView()
        this.ckecked()}}>
          <Text style={{ color: 'white' }}>Send Data To WebView from React Native</Text>
        </TouchableHighlight>
        <TouchableHighlight style={{ padding: 10, backgroundColor: 'gray', marginTop: 20 }} onPress={() => this.ckecked()}>
          <Text style={{ color: 'white' }}>Check radio</Text>
        </TouchableHighlight>

        <WebView
          onMessage = {(event)=>{}}
          injectedJavaScript={injectJs}
          style={{ flex: 1 }}
          source={{ uri: 'https://www.dgca.gov.in/digigov-portal/web?requestType=ApplicationRH&actionVal=checkLogin' }}
          ref={(webView) => this.webView = webView}

        />

      </View>
    );
  }
}